## Jira/Sentry:
https://jira.zitcom.dk or https://sentry1.dk.team.blue/

## Description:
The first line of a CL description should be a short summary of specifically **what** is being done by the CL(ChangeList), followed by a blank line. fx *Deleting the FizzBuzz RPC and replacing it with the new system.*

The rest of the description should be informative. It might include a brief description of the problem that’s being solved, and why this is the best approach.

## Screenshots:
Add screenshots of new visual features here, if relevant.

## Test:
Add information about the elements that require extra testing.

## Automated testing:
* [ ] I've run the automated testing related to this branch/feature/update

*Guidelines for MR: [Guide](https://google.github.io/eng-practices/review/developer/cl-descriptions.html)*
