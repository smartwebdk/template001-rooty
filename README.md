# Rooty theme #
============

[![Code Climate](https://codeclimate.com/repos/5748378e0b1db466ff0051d9/badges/3149a2790d65a7cdacf6/gpa.svg)](https://codeclimate.com/repos/5748378e0b1db466ff0051d9/feed)

The Rooty theme is the main theme of SmartWeb, containing all template files, which sub-templates inherits part of. It can be used as a starting point for theme designers.

**Demo:**

* [Demo store](http://rooty-theme.smartweb.dk/)


## Documentation

* [Getting started - in Danish](http://design-help-new.smart-web.dk/35-template-udvikling/).
* [Theme Documentation - in Danish](http://design-help-new.smart-web.dk/3-for-udviklere/).
* Need more help? Contact our [Support](http://www.get-smartweb.com/support/).